//Завдання 1
function Human(name, age, value) {
	this.name = name;
	this.age = age;
	this.value = value;
}

let arr = [];
arr.push(new Human("Yura", 27, 5500));
arr.push(new Human("Vlad", 22, 4500));
arr.push(new Human("Ihor", 32, 8000));
arr.push(new Human("Max", 18, 2500));

arr.sort((a, b)=> a.age - b.age)
console.log(arr)

//Завдання 2

function Human1(name, age, value) {
	this.name = name;
	this.age = age;
	this.value = value;
}

let arr1 = [];
arr1.push(new Human("Natasha", 33, 5500));
arr1.push(new Human("Yana", 12, 4500));
arr1.push(new Human("Vika", 17, 8000));
arr1.push(new Human("Nastia", 18, 2500));

let sorting = arr1.filter(old => old.age >=18);


console.log(sorting)